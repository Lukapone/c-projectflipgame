﻿using FlipGame.MainApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipGame.GameClasses
{
    public class Player
    {

        private DrawableEntityManager gameManager;
        private Texture2D texture;
        private Vector2 position;



        public Player(DrawableEntityManager gM)
        {
            this.gameManager = gM;
        }

        public void Initialize()
        {
            position = new Vector2(300, 200);
           
        }
        public void LoadContent()
        {
            texture = this.gameManager.Game.Content.Load<Texture2D>("Images/Textures/Sphere");
        }

        public void Update(GameTime gameTime)
        {
            if(Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                position.X -= 2f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                position.X += 2f;
            }
        }

        public void Draw(GameTime gameTime,SpriteBatch sb)
        {
            sb.Draw(texture, position, Color.White);
        }





    }
}
