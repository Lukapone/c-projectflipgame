﻿using FlipGame.Managers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipGame.GameClasses
{
    public class GameScreen
    {


        private ScreenManager screenManager;

        
        private bool isActive;
        //constructors
        public GameScreen(ScreenManager sM)
        {
            this.screenManager = sM; 
            this.isActive = true;
        }

        //getters and setters
        public ScreenManager ScreenManager
        {
            get { return screenManager; }
        }

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }


        //methods
        public void Initialize()
        {
          

        }
 
        public void Update(GameTime gameTime)
        {
            screenManager.GameManager.Player.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {

            screenManager.SpriteBatch.Begin();

            screenManager.GameManager.Player.Draw(gameTime, screenManager.SpriteBatch);

            screenManager.SpriteBatch.End();

        }

      

    }
}
