﻿using FlipGame.GameClasses;
using FlipGame.MainApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipGame.Managers
{
    public class ScreenManager
    {
        private DrawableEntityManager gameManager;
        private List<GameScreen> screens;
        private SpriteBatch spriteBatch;


        public ScreenManager(DrawableEntityManager gM)
        {
            this.gameManager = gM;
            this.screens = new List<GameScreen>();
        }

        //getters and setters
        public List<GameScreen> Screens
        {
            get { return screens; }
        }

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public DrawableEntityManager GameManager
        {
            get { return gameManager; }
        }



        //methods
        public void addScreen(GameScreen p_screen)
        {
            if (!screens.Contains(p_screen))
            {
                screens.Add(p_screen);
            }
        }

        public void removeScreen(GameScreen p_screen)
        {
            if (screens.Contains(p_screen))
            {
                screens.Remove(p_screen);
            }
        }




        public void Initialize()
        {
            spriteBatch = new SpriteBatch(this.gameManager.GraphicsDevice);
        }


        public void LoadContent()
        {

        }

        public void Update(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.IsActive)
                {
                    screen.Update(gameTime);
                }
            }
        }

        public void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.IsActive)
                {
                    screen.Draw(gameTime);
                }
            }
        }
    }

}
