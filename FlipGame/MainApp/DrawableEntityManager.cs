﻿using FlipGame.GameClasses;
using FlipGame.Managers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//class which will manage all drawable game comnponents
namespace FlipGame.MainApp
{
    public class DrawableEntityManager : DrawableGameComponent
    {

        private Player player;
        private ScreenManager screenManager;

        //constructors
        public DrawableEntityManager(Game game)
            :base(game)
        {
            player = new Player(this);
            screenManager = new ScreenManager(this);
        }

        //getters and setters

        public Player Player
        {
            get { return player; }
        }

        public override void Initialize()
        {
            //Console.WriteLine("GC init");

            
            base.Initialize();
            screenManager.Initialize();
            screenManager.addScreen(new GameScreen(screenManager));
            player.Initialize();
        }
        protected override void LoadContent()
        {
            //Console.WriteLine("GC load content");
            screenManager.LoadContent();
            player.LoadContent();
            base.LoadContent();
        }

        //protected override void UnloadContent()
        //{
        //    base.UnloadContent();
        //}

        public override void Update(GameTime gameTime)
        {
            //Console.WriteLine("GC update");
            screenManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            this.GraphicsDevice.Clear(Color.CornflowerBlue);

            screenManager.Draw(gameTime);
            //Console.WriteLine("GC draw");
            base.Draw(gameTime);
        }

    }
}
